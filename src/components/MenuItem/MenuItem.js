import React from 'react';
import './MenuItem.css';

const MenuItem = props => {
  return (<div className='Item' onClick={props.click}>
    {props.children}
  </div>);
};

export default MenuItem;