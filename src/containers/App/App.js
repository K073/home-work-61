import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import Menu from "../../components/Menu/Menu";
import Info from "../../components/Info/Info";

const URL_COUNTRIES = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
const URL_COUNTRY = 'https://restcountries.eu/rest/v2/alpha/';

class App extends Component {
  state = {
    countryList: [],
    fullCountryInfo: {},
    bordersCountry: []
  };

  getCountriesList = () => {
    axios.get(URL_COUNTRIES).then(res => {
      this.setState(prevState => {
        return prevState.countryList = res.data;
      });
    })
  };

  getCountryInfo = code => {
    axios.get(URL_COUNTRY + code).then(res => {
      this.setState(prevState => {
        return prevState.fullCountryInfo = res.data
      });
      this.getCounryBorders(res.data.borders)
    })
  };

  getCounryBorders = arrCode => {
    axios.all(arrCode.map(value => {
      return axios.get(URL_COUNTRY + value + '?fields=name;alpha3Code')
    })).then(res => {
      const data = res.map(value => value.data);
      this.setState(prevState => {
        return prevState.bordersCountry = data;
      })
    })
  };

  componentDidMount () {
    this.getCountriesList()
  };

  render() {
    return (
      <div className="App">
        <Menu click={this.getCountryInfo} list={this.state.countryList}/>
        <Info value={this.state.fullCountryInfo.name}  list={this.state.bordersCountry} />
      </div>
    );
  }
}

export default App;
