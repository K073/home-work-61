import React from 'react';
import MenuItem from "../MenuItem/MenuItem";
import './Menu.css';

const Menu = props => {
  return (
    <div className="Menu">
      {props.list.map(value => <MenuItem key={value.alpha3Code} click={() => props.click(value.alpha3Code)} >{value.name}</MenuItem>)}
    </div>
  );
};

export default Menu