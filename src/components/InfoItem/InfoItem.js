import React from 'react';

const InfoItem = props => {
  return(
    <div>{props.children}</div>
  );
};

export default InfoItem;