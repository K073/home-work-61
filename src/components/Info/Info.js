import React from 'react';
import InfoItem from "../InfoItem/InfoItem";
import './Info.css';

const Info = props => {
  return(
    <div className='Info'>
      <div>{props.value}</div>
      {props.list.map(value => <InfoItem key={value.alpha3Code}>{value.name}</InfoItem>)}
    </div>
  );
};

export default Info;